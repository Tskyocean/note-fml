#unity UI交互
###1.场景跳转
####SceneManager.LoadScene("Scene");
###2.实现按T移动，再按T复原
```cs
public class UIskip : MonoBehaviour
{ 
    public Button enterButton;
    public bool isMoving=false;
    public GameObject view;
    void start
    {

    }
    void  Update
    {
     if (Input.GetKeyUp(KeyCode.T))
        {
            if (!isMoving)
            {
                goMove();
                isMoving = true;
            }
            else if (isMoving)
            {
                backMove();
                isMoving = false;
            }
        }
    }  
     public void goMove()
    {
        view.transform.DOLocalMove(new Vector3(0, 0, 0), 1);
    }
    public void backMove()
    {
        view.transform.DOLocalMove(new Vector3(0, 0, 0), 1);
    }
}
```
###3.设置划移镜头
#####引入镜头脚本（素材与资源）
修改camera rover 中的相机位置
![Alt text](../img/2023-07-17_104825.jpg)

###4.射线碰撞
#####物体不要忘记添加碰撞组件
``` 
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit) && hit.collider.gameObject == targetObject)
            {             
                //显示进入按钮
                enterButton.gameObject.SetActive(true);
            }
        }
    }
```
###5.拖拽窗口
```cs
using UnityEngine;
using UnityEngine.EventSystems;

public class DragWindow : MonoBehaviour, IPointerDownHandler, IDragHandler
{
    private Vector2 localMousePos;
    private Vector3 planeLocalPos;
    private RectTransform targetObject;
    private RectTransform parentRectTransform;
    private RectTransform targetRectTransform;

    void Awake()
    {
        targetObject = this.transform.GetComponent<RectTransform>();
        targetRectTransform = targetObject;
        parentRectTransform = targetObject.parent as RectTransform;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        planeLocalPos = targetRectTransform.localPosition;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, eventData.position, eventData.pressEventCamera, out localMousePos);
        targetObject.gameObject.transform.SetAsLastSibling();
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 localPointerPosition;
        //屏幕点到矩形中的局部点
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, eventData.position, eventData.pressEventCamera, out localPointerPosition))
        {
            Vector3 offsetToOriginal = localPointerPosition - localMousePos;
            targetObject.localPosition = planeLocalPos + offsetToOriginal;
        }
    }
}
```
###鼠标点击碰撞检测
#####target关联相关物体并且添加碰撞体
```cs
  void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit) && hit.collider.gameObject == targetObject1)
            {
                cameravideoShow();
            }
            else if (Physics.Raycast(ray, out hit) && hit.collider.gameObject == targetObject2)
            {
                cameravideoShow2();
            };
        }
    }
```