#Unity C# 编程知识点
###1.Instantiate
```cs
 Instantiate(rocketPrefab, barrelEnd.position,barrelEnd.rotation) 
```
######把实例化的对象转化为rigidbody，并加一个力
```cs
    public Rigidbody rocketPrefab;
    public Transform barrelEnd;   
    void Update ()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            Rigidbody rocketInstance;
            rocketInstance = Instantiate(rocketPrefab, barrelEnd.position, barrelEnd.rotation) as Rigidbody;
            rocketInstance.AddForce(barrelEnd.forward * 5000);
        }
    }
```
###2.Invoke
```cs
 Invoke ("Object", 2);
 InvokeRepeating("Object", 2, 1);
 CancelInvoke("Object");
```
###3.退出游戏
```
    public void EndingGame()
    {
        UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }
```
###4.Static 
```cs
using UnityEngine;
using System.Collections;

public class Enemy
{
    //静态变量是在类的所有实例之间
    //共享的变量。
    public static int enemyCount = 0;
    public Enemy()
    {
        //通过递增静态变量了解
        //已创建此类的多少个对象。
        enemyCount++;
    }
}
```
```cs
using UnityEngine;
using System.Collections;

public class Game
{
    void Start () 
    {
        Enemy enemy1 = new Enemy();
        Enemy enemy2 = new Enemy();
        Enemy enemy3 = new Enemy();
        //可以使用类名和点运算符
        //来访问静态变量。
        int x = Enemy.enemyCount;
    }
}
```
```cs
using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
    //静态变量是在类的所有实例之间
    //共享的变量。
    public static int playerCount = 0;

    void Start()
    {
        //通过递增静态变量了解
         //已创建此类的多少个对象。
        playerCount++;
    }
}
```
```cs
using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour 
{
    void Start()
    {
        //可以使用类名和点运算符
        //来访问静态变量。
        int x = Player.playerCount;
    }
}

```
```cs
using UnityEngine;
using System.Collections;

public static class Utilities 
{
    //可以在没有类对象的情况下调用
    //静态方法。请注意，静态方法无法访问
    //非静态成员变量。
    public static int Add(int num1, int num2)
    {
        return num1 + num2;
    }
}
```
```cs
using UnityEngine;
using System.Collections;

public class UtilitiesExample : MonoBehaviour 
{
    void Start()
    {
        //可以使用类名和点运算符
        //来访问静态方法。
        int x = Utilities.Add (5, 6);
    }
}
```

###出现NullReferenceException:Object reference not set to an instance of an object.的原因总结
#####1）. 物体没有激活
①运行前物体没有被激活，导致运行时找不到该物体；
②运行时物体被脚本控制取消了激活，导致用到该物体时找不到。
#####2）. 物体的父物体、祖父物体……没有激活
Unity中，如果一个物体的父物体没有激活，那么其所有子物体是找不到的。
类似的，这种情况也会导致运行时找不到物体。
尤其是使用GameObject.Find()函数全局查找GameObject的时候需要尤其注意。
#####3）. 没有挂载脚本
继承于MonoBehaviour的脚本没有挂载到Hierarchy面板上的任何GameObject。
这种情况下脚本中的Awake(),Start()和Update()脚本就都不会被执行，因此自然找不到需要的物体。
#####4）. 追祖溯源
对于没有继承于MonoBehaviour的脚本，需要查看调用该脚本的源头脚本有没有运行（即其被挂载到了Hierarchy面板上的某个物体，且其脚本组件被激活）
#####5）. 资源加载失败
脚本中使用到了Resources.Load()函数，然而其所指向的路径中没有对应的东西。
查看Assets文件夹下的Resources文件夹下是都有对应加载的资源。
#####6）. 变量未赋初值
只定义了变量，但没有为其赋初值。
######如果改变了初始Camera的Tag，要重新添加 MainCamera。
```cs
public Vector3 vec = new Vector3(0,0,0);
public string[] strs = new string[5];
int index = 0;
float ff = 0.0f;
```


只定义了脚本实例，但没有实例化它。
如定义了继承于Monobehavior地脚本的一个实例Demo，需要在其Awake时将其实例化，才能在其他地方引用其方法时不报空。

```cs
public class Demo : MonoBehaviour
{
    public static Demo Instance;

    private void Awake()
    {
        Instance = this;
    }
}
```