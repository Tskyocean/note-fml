#unity Dowteen
##1.tween
####引用 using DG.Tweening;
####移动
```cs
transform.DOMove(Vector3.one, 2);//2秒移动到（1，1，1）位置
transform.DOMoveX(3f,2);//x移动到3.0位置
transform.DOLocalMove(Vector3.one, 2);//按自身坐标系移动到目标位置
transform.DORotate(Vector3(20,20,20), 2);//2秒旋转到（30，30，30）的角度
transform.DOScale(Vector3(20,20,20),2);//将物体的Scale2秒缩放到（20，20，20）
transform.DOMoveX(3f,2).Setdelay(0.1f);//推迟0.1秒
```
####过渡效果
######使sphere在1秒内向x轴相对 SetRelative() 的移动了+20的位置。中间的 SetEase() 函数确定了使用那个缓动函数，可以看出不同的缓动函数可以实现不同的动画过度效果
```cs
sphere1.DOMoveX(20,1).SetEase(Ease.OutBack).SetRelative();
sphere2.DOMoveX(20,1).SetEase(Ease.InQuad).SetRelative();
sphere3.DOMoveX(20,1).SetEase(Ease.InOutQuad).SetRelative();
sphere4.DOMoveX(20,1).SetEase(Ease.Linear).SetRelative();
sphere5.DOMoveX(20,1).SetEase(Ease.InOutCubic).SetRelative();
```
####多个弹窗动画
```cs
public Transform[] items = new Transform[4];
void Start()
{
     StartCoroutine(Show());
}
IEnumerator Show()
{   //items数组引用了图片中的那几个长条Transform
    foreach(var item in items)
    {
        item.DOLocalMoveX(-1000,1f,false).From().SetEase(Ease.OutBack);
        yield return new WaitForSeconds(0.02f);
    }
}
```
####相机动画控制
```cs
Camera cam = Camera.main;//宽高比
cam.DOAspect(16 / 9,2);//2秒时间将相机宽高比变为16：9颜色
cam.DOColor(Color.red, 2);//2秒时间将相机颜色变到红色，对应相机的Background属性远近截面
cam.DONearClipPlane(1f, 2);//2秒时间将相机近截面变到1，对应相机的clipping Plane Near的属性
cam.DOFarClipPlane(10f, 2);//2秒时间将相机远截面变到10，对应相机的clipping Plane Near的属性相机视域
cam.DOFieldOfView(100, 2);//2秒时间将透视相机的FieldOfView属性变为100
cam.DOOrthoSize(10, 2);//2秒时间将正交相机的Size属性变为100分屏
cam.DORect(new Rect(0, 0, 0.5f, 0.5f), 2);//2秒时间将相机的Viewport Rect属性变为(0,0,0.5f,0.5f)
cam.DOPixelRect(new Rect(0, 0, 960, 540), 3f);//2秒时间将相机根据像素设置Viewport Rect属性为(0,0,960/相机的宽,540/相机的高)相机震动
cam.DOShakePosition(3f, 1, 10, 90);//在3秒时间内相机以强度为1、随机角度为90度震动10次
```


####0rbitCamera脚本（素材与资源）
######Reset方法
```cs
control.Reset(0,0,10);//可以通过事件跳转相机位置
```

##2.sequence
##3.