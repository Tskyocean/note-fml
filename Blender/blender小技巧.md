###blender小技巧
####1.倒角
Ctrl+B倒角，鼠标滚轮增加段数![Alt text](img/Snipaste_2023-08-22_09-00-44.png)
属性面板可自定义段数，步数阶梯![Alt text](img/Snipaste_2023-08-22_09-03-23.png)
####2.对齐（安装插件）在右边栏
先框选所有（橙红），按shift点击要对齐的参考物（橙黄），最后在面板中选择
![Alt text](img/Snipaste_2023-08-22_10-19-04.png)
####3.复制
shift+D 复制物体（独立object）
shift+R*5 重复且等距复制
![Alt text](img/Snipaste_2023-08-22_10-30-02.png)
AIT+D 复制物体（类似prefab）
在tab模式下改变物体，复制物也会跟着改变
![Alt text](img/Snipaste_2023-08-22_11-22-25.png)
Alt+D R旋转 Ctrl固定角度 shift+R*6 可旋转复制
![Alt text](img/Snipaste_2023-08-22_11-57-18.png)
阵列修改器（可添加多个）
apply塌陷后，tab编辑模式下选择所有，P键选择（by loose path）按松散块，就可以分离所有物体
选中所有→右键→设置原点→原点→几何中心，原点就可以回到各自的中心
![Alt text](img/Snipaste_2023-08-22_13-10-48.png)
框选所有→Ctrl+L→关联物体数据
![Alt text](img/Snipaste_2023-08-22_14-07-04.png)
解绑，点击属性面板物体名称后的数字即可
![Alt text](img/Snipaste_2023-08-22_14-14-52.png)
####4.桥接 Ctrl+E
选择同一个物体的两个线或面Ctrl+E
混合曲线与切割次数
![Alt text](img/Snipaste_2023-08-30_10-37-42.png)
####5.曲线
数据属性面板 | 几何数据挤出，深度
Alt+S可控制管道截面大小；轮廓可控制截面形状；
![Alt text](img/Snipaste_2023-08-23_10-49-11.png)
开始结束映射 ：可根据路径K帧播放![Alt text](img/Snipaste_2023-08-23_11-01-20.png)
####6.阵列与曲线（导管）
创建物体圆柱与曲线，添加阵列→曲线，保持形变轴一致，轴心一致，勾选合并
选择适配曲线，吸附可直接创建匹配长度
![Alt text](img/Snipaste_2023-08-30_10-46-41.png)
####6.编织（Tissue插件）
gi